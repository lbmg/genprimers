import wx
import wx.lib.scrolledpanel as scrolled



class windowClass(wx.Frame):

	def __init__(self, parent, title):

		wx.Frame.__init__(self, parent, title=title,size=(630,320))
		
		global panel
		global db_button
		# panel = wx.Panel(self)

		panel = scrolled.ScrolledPanel(self)
		panel.SetAutoLayout(1)
		panel.SetupScrolling()

		box_1 = wx.StaticBox(panel, label="File Segment")


		welcome  = wx.StaticText(panel,-1,label ="WELCOME TO GENPRIMERS")
		info  = wx.StaticText(panel,-1,label ="Please complete the info listed below")


		top_layout=wx.BoxSizer(wx.VERTICAL)

		top_layout.Add(welcome,0,wx.CENTER|wx.ALL,border=10)
		top_layout.Add(info,0,wx.CENTER|wx.ALL,border=10)



		grid_box = wx.GridBagSizer(3,5) #creates a grid in format ( Y, X ) or (Row , Columns)
		
		db_file = wx.StaticText(panel,-1,label ="DataBase file ")
		db_button = wx.FilePickerCtrl(panel,wx.ID_ANY,size=(400,30))
		#self.Bind(wx.EVT_BUTTON,self.onClickDBFILE,db_button)

		#db_text = wx.TextCtrl(panel,size=(150,20))
		#print "asdasd"


		grid_box.Add(db_file,pos=(0,0), flag=wx.TOP|wx.RIGHT|wx.BOTTOM,border=5)
		grid_box.Add(db_button,pos=(0,1),span=(1,4))
		#grid_box.Add(db_text,pos=(0,1), span=(1,4))



		targets_file = wx.StaticText(panel,-1,label ="Targets id file ")
		targets_button= wx.FilePickerCtrl(panel,wx.ID_ANY,size=(400,30))		#targets_button.Bind(wx.EVT_BUTTON,self.onClickTGFILE,targets_button)


		#targets_text = wx.TextCtrl(panel,size=(150,20))

		grid_box.Add(targets_file, pos=(1,0), flag=wx.TOP|wx.RIGHT|wx.BOTTOM,border=5)
		grid_box.Add(targets_button, pos=(1,1),span=(1,4))
		#grid_box.Add(targets_text, pos=(1,1), span=(1,4))


		output_dir = wx.StaticText(panel,-1,label="Choose output folder")
		output_button= wx.DirPickerCtrl(panel,wx.ID_ANY,size=(400,30))
		#output_button.Bind(wx.EVT_BUTTON,self.onClickTGFILE,targets_button)
		#output_text = wx.TextCtrl(panel, size=(150,20))

		grid_box.Add(output_dir, pos=(2,0), flag=wx.TOP|wx.RIGHT|wx.BOTTOM,border=5)
		grid_box.Add(output_button, pos=(2,1),span=(1,4))
		#grid_box.Add(output_text,pos=(2,1), span=(1,4))




		line1 = wx.StaticLine(panel)


#		box_1 = wx.StaticBox(panel, label="File Segment")


		box_sizer = wx.StaticBoxSizer(box_1, wx.VERTICAL) #agregar la caja
		box_sizer.Add(grid_box,0,wx.CENTER|wx.ALL,border=10)


		top_layout.Add(box_sizer,0,wx.CENTER|wx.ALL,border=10)


########## LOS BOTONES PRINCIPALES ############

		global box_4
		global box_sizer_4
		global grid_box_5
		global opr_temp
		global opr_temp_button
		global mpc
		global mpc_button
		global mcm
		global mcm_button
		global dcm
		global dcm_button
		global sdss
		global sdss_button
		global line3
		global energy_pair
		global energy_pair_button
		global confidence
		global confidence_button
		global min_lc
		global min_lc_button
		global line2
		global mismatches
		global mismatches_button
		global max_gc
		global max_gc_button
		global min_gc
		global min_gc_button
		global cutoff_homodimer
		global cutoff_homodimer_button
		global cutoff_hairpin
		global cutoff_hairpin_button
		global max_melt_temp
		global max_melt_temp_button
		global min_melt_temp
		global min_melt_temp_button
		global empty_space2
		global max_prim_size
		global max_prim_size_button
		global min_prim_size
		global min_prim_size_button
		global box_2
		global min_amp
		global min_amp_button
		global box_3
		global max_amp_button


		grid_box_2 = wx.GridBagSizer(1,5)
		exit_button = wx.Button(panel, -1,"Exit")
		global cont_button
		cont_button = wx.Button(panel, -1,label="More Options")
		cont_button.Bind(wx.EVT_BUTTON, self.on_options, cont_button)
		run_button = wx.Button(panel, -1,"RUN")
		self.Bind(wx.EVT_BUTTON,self.onClickRUN,run_button)

		empty_space = wx.StaticText(panel,-1,label=" ", size=(140,20) )
		
		grid_box_2.Add(exit_button, pos =(0,0),flag=wx.LEFT,border =20)
		grid_box_2.Add(empty_space, pos=(0,1), span=(1,2))
		grid_box_2.Add(cont_button, pos =(0,3),flag=wx.RIGHT,border =5)
		grid_box_2.Add(run_button, pos =(0,4),flag=wx.RIGHT,border =5)

		top_layout.Add(grid_box_2,0,wx.CENTER,border=20)
		empty_space_0 = wx.StaticText(panel,-1,label=" ")
		top_layout.Add(empty_space_0,0,wx.CENTER,border=20)

		top_layout.Add(line1,flag=wx.EXPAND|wx.ALL)



######### BOTONES OPCIONES EXTRA ##############


		#botton_panel=wx.BoxSizer(wx.HORIZONTAL)
		#left_bot_panel=wx.BoxSizer(wx.VERTICAL)


########### BOX 2 EXTRA OPTIONS ################

		grid_box_3 = wx.GridBagSizer(7,5)

		box_2 = wx.StaticBox(panel, label="Primer Spec")
		box_sizer_2 = wx.StaticBoxSizer(box_2, wx.VERTICAL)
		box_sizer_2.Add(grid_box_3,0,wx.CENTER|wx.ALL, border=10)

		min_prim_size = wx.StaticText(panel,-1,label="Min Primer Size")
		min_prim_size_button = wx.SpinCtrl(panel,-1,value="minps",min=10,max=28,initial=18,name='min_spinner')

		grid_box_3.Add(min_prim_size, pos=(0,0), flag=wx.LEFT,border=5)
		grid_box_3.Add(min_prim_size_button,pos=(0,1))

		max_prim_size = wx.StaticText(panel,-1,label="Max Primer Size")
		max_prim_size_button = wx.SpinCtrl(panel,-1,value="maxps",min=16,max=34,initial=22,name='max_spinner')

		grid_box_3.Add(max_prim_size, pos=(1,0), flag=wx.LEFT,border=5)
		grid_box_3.Add(max_prim_size_button,pos=(1,1))


		empty_space2=wx.StaticText(panel,-1,label=" ",size=(10,10))
		grid_box_3.Add(empty_space2,pos=(2,0),span=(1,5))



		min_melt_temp = wx.StaticText(panel,-1,label="Min Temp Melting (C)")
		min_melt_temp_button = wx.SpinCtrl(panel,-1,value="mintm",min=20,max=120,initial=60,name='min_tm_spinner') 

		grid_box_3.Add(min_melt_temp,pos=(3,0),flag=wx.LEFT, border=5)
		grid_box_3.Add(min_melt_temp_button,pos=(3,1))

		max_melt_temp = wx.StaticText(panel,-1,label="Max Temp Melting (C)")
		max_melt_temp_button = wx.SpinCtrl(panel,-1,value="maxtm",min=20,max=120,initial=65,name='max_tm_spinner') 

		grid_box_3.Add(max_melt_temp,pos=(4,0),flag=wx.LEFT, border=5)
		grid_box_3.Add(max_melt_temp_button,pos=(4,1))


#### ====>

		cutoff_hairpin = wx.StaticText(panel,-1,label="Cuttof Hairpin kcal/mol")
		cutoff_hairpin_button = wx.SpinCtrlDouble(panel,-1,value="mincha",min=-5,max=0,initial=-1.5,inc=0.1,name='min_hair_spinner')

		grid_box_3.Add(cutoff_hairpin, pos=(0,3),flag=wx.RIGHT, border = 5)
		grid_box_3.Add(cutoff_hairpin_button,pos=(0,4))

		cutoff_homodimer = wx.StaticText(panel,-1,label="Cuttof Homodimer kcal/mol")
		cutoff_homodimer_button = wx.SpinCtrlDouble(panel,-1,value="maxcha",min=-20,max=0,initial=-8.5,inc=0.1,name='max_hair_spinner')

		grid_box_3.Add(cutoff_homodimer, pos=(1,3),flag=wx.RIGHT, border = 5)
		grid_box_3.Add(cutoff_homodimer_button,pos=(1,4))

		min_gc = wx.StaticText(panel,-1,label="Min GC content (%)")
		min_gc_button = wx.SpinCtrl(panel,-1,value="mingc",min=-0,max=90,initial=30,name='min_gc_spinner')

		grid_box_3.Add(min_gc, pos=(3,3),flag=wx.RIGHT, border = 5)
		grid_box_3.Add(min_gc_button,pos=(3,4))

		max_gc = wx.StaticText(panel,-1,label="Max GC content (%)")
		max_gc_button = wx.SpinCtrl(panel,-1,value="maxgc",min=10,max=100,initial=70,name='max_gc_spinner')

		grid_box_3.Add(max_gc, pos=(4,3),flag=wx.RIGHT, border = 5)
		grid_box_3.Add(max_gc_button,pos=(4,4))

# ||
# ||
# \/	

		min_lc = wx.StaticText(panel,-1,label="Min Low Complexity")
		min_lc_button = wx.SpinCtrlDouble(panel,-1,value="mlc",min=1,max=10,initial=1,inc=0.5,name='min_lc_spinner')

		grid_box_3.Add(min_lc, pos=(6,0),flag=wx.LEFT, border = 5)
		grid_box_3.Add(min_lc_button,pos=(6,1))

		mismatches = wx.StaticText(panel,-1,label="Max Mismatches allowed")
		mismatches_button = wx.SpinCtrl(panel,-1,value="mm",min=0,max=3,initial=0,name='max_mismatches_spinner')

		grid_box_3.Add(mismatches, pos=(6,3),flag=wx.RIGHT, border = 5)
		grid_box_3.Add(mismatches_button,pos=(6,4))


		#botton_panel.Add(box_sizer_2,0,wx.CENTER|wx.ALL,border=10)
		line2= wx.StaticLine(panel)
		top_layout.Add(box_sizer_2,0,wx.CENTER|wx.ALL,border=10)
		top_layout.Add(line2,flag=wx.EXPAND|wx.ALL)

########### BOX 3 EXTRA OPTIONS ################

		#left_bot_panel=wx.BoxSizer(wx.VERTICAL)

		grid_box_4 = wx.GridBagSizer(2,5)

		box_3 = wx.StaticBox(panel, label="Primer Pair Spec")
		box_sizer_3 = wx.StaticBoxSizer(box_3, wx.VERTICAL)
		box_sizer_3.Add(grid_box_4,0,wx.CENTER|wx.ALL, border=10)



		min_amp = wx.StaticText(panel,-1,label="Min amplicon size (pb)")
		min_amp_button = wx.SpinCtrl(panel,-1,value="minas",min=20,max=10000,initial=100,name='min_amp_spinner')

		grid_box_4.Add(min_amp, pos=(0,0), flag=wx.LEFT, border=10)
		grid_box_4.Add(min_amp_button, pos=(0,1))

		max_amp = wx.StaticText(panel,-1,label="Max amplicon size (pb)")
		max_amp_button = wx.SpinCtrl(panel,-1,value="maxas",min=30,max=10000,initial=300,name='max_amp_spinner')

		grid_box_4.Add(max_amp, pos=(1,0), flag=wx.LEFT, border=10)
		grid_box_4.Add(max_amp_button, pos=(1,1))

#### ====> 

		confidence = wx.StaticText(panel,-1,label="Confidence (1 = 100%)")
		confidence_button = wx.SpinCtrlDouble(panel,-1,value="confi",min=0,max=1,initial=0.9,inc=0.05,name='confidence_spinner')

		grid_box_4.Add(confidence, pos=(0,3), flag=wx.LEFT, border=10)
		grid_box_4.Add(confidence_button, pos=(0,4))

		energy_pair = wx.StaticText(panel,-1,label="Min Energy Pair kcal/mol")
		energy_pair_button = wx.SpinCtrlDouble(panel,-1,value="enpa",min=-20,max=1,initial=-8.5,inc=0.2,name='energy_pair_spinner')

		grid_box_4.Add(energy_pair, pos=(1,3), flag=wx.LEFT, border=10)
		grid_box_4.Add(energy_pair_button, pos=(1,4))



		#left_bot_panel.Add(box_sizer_3,0,wx.CENTER|wx.ALL,border=10)
		line3= wx.StaticLine(panel)

		top_layout.Add(box_sizer_3,0,wx.CENTER|wx.ALL,border=10)
		top_layout.Add(line3,flag=wx.EXPAND|wx.ALL)



########### BOX 4 EXTRA OPTIONS ################


		grid_box_5 = wx.GridBagSizer(5,5)



		box_4 = wx.StaticBox(panel, label="Thermodynamic parameters")
		box_sizer_4 = wx.StaticBoxSizer(box_4, wx.VERTICAL)
		box_sizer_4.Add(grid_box_5,0,wx.CENTER|wx.ALL, border=10)

		opr_temp = wx.StaticText(panel,-1,label="PCR Operation Temp (C)")
		opr_temp_button = wx.SpinCtrl(panel,-1,value="opr",min=0,max=120,initial=50,name='pcr_temp_spinner')

		grid_box_5.Add(opr_temp, pos=(0,0), flag=wx.LEFT, border=10)
		grid_box_5.Add(opr_temp_button, pos=(0,1))


		mpc = wx.StaticText(panel,-1,label="Molar Primer Concentration")
		mpc_button = wx.SpinCtrlDouble(panel,-1,value="mpc",min=0.00000002,max=0.00002,initial=0.0000002,inc=0.000000001,name='mol_prim_spinner')

		grid_box_5.Add(mpc, pos=(2,0), flag=wx.LEFT, border=10)
		grid_box_5.Add(mpc_button, pos=(2,1))

		mcm = wx.StaticText(panel,-1,label="Monovalent Cation Molar")
		mcm_button = wx.SpinCtrlDouble(panel,-1,value="moco",min=0.00005,max=0.5,initial=0.005,inc=0.0001,name='mono_con_spinner')

		grid_box_5.Add(mcm, pos=(4,0), flag=wx.LEFT, border=10)
		grid_box_5.Add(mcm_button, pos=(4,1))

		dcm = wx.StaticText(panel,-1,label="Divalent Cation Molar")
		dcm_button = wx.SpinCtrlDouble(panel,-1,value="dico",min=0,max=0.002,initial=0,inc=0.001,name='diva_con_spinner')

		grid_box_5.Add(dcm, pos=(1,3), flag=wx.LEFT, border=10)
		grid_box_5.Add(dcm_button, pos=(1,4))

		sdss = wx.StaticText(panel,-1,label="SDSS Threshold")
		sdss_button = wx.SpinCtrlDouble(panel,-1,value="sdss",min=0,max=0.1,initial=0.01,inc=0.001,name='sdss_cut_spinner')

		grid_box_5.Add(sdss, pos=(3,3), flag=wx.LEFT, border=10)
		grid_box_5.Add(sdss_button, pos=(3,4))



		#box_sizer_4.Add(grid_box_5,0,wx.CENTER|wx.ALL, border=10)
		top_layout.Add(box_sizer_4,0,wx.CENTER|wx.ALL, border=10)
		global line4
		line4= wx.StaticLine(panel)
		top_layout.Add(line4,flag=wx.EXPAND|wx.ALL)

		#left_bot_panel.Add(box_sizer_4,0,wx.CENTER|wx.ALL,border=10)


######### HIDE ALL CUSTOM OPTIONS #######################





######### ADD ALL PANELS TO THE MAIN WINDOW ################

		#botton_panel.Add(left_bot_panel,0,wx.CENTER|wx.ALL,border=10)
		#botton_panel.Add(left_bot_panel,0,wx.CENTER|wx.ALL,border=10)

		#top_layout.Add(botton_panel,0,wx.CENTER|wx.ALL,border=10)

		
		panel.SetSizer(top_layout)

		self.SetTitle("Genprimers V2.0")
		self.Show(True)
		self.SetMinSize(size=(630,320))
		box_4.Hide()
		opr_temp.Hide()
		opr_temp_button.Hide()
		mpc.Hide()
		mpc_button.Hide()
		mcm.Hide()
		mcm_button.Hide()
		dcm.Hide()
		dcm_button.Hide()
		sdss.Hide()
		sdss_button.Hide()
		line4.Hide()
		line3.Hide()
		energy_pair.Hide()
		energy_pair_button.Hide()
		confidence.Hide()
		confidence_button.Hide()
		min_lc.Hide()
		min_lc_button.Hide()
		line2.Hide()
		mismatches.Hide()
		max_gc.Hide()
		max_gc_button.Hide()
		min_gc.Hide()
		min_gc_button.Hide()
		cutoff_homodimer.Hide()
		cutoff_homodimer_button.Hide()
		cutoff_hairpin.Hide()
		cutoff_hairpin_button.Hide()
		max_melt_temp.Hide()
		max_melt_temp_button.Hide()
		min_melt_temp.Hide()
		min_melt_temp_button.Hide()
		empty_space2.Hide()
		max_prim_size.Hide()
		max_prim_size_button.Hide()
		min_prim_size.Hide()
		min_prim_size_button.Hide()
		box_2.Hide()
		min_amp.Hide()
		min_amp_button.Hide()
		max_amp_button.Hide()
		mismatches_button.Hide()
		box_3.Hide()


	def on_options(self, e):

		if cont_button.GetLabel() == "More Options":
			box_4.Show()
			opr_temp.Show()
			opr_temp_button.Show()
			mpc.Show()
			mpc_button.Show()
			mcm.Show()
			mcm_button.Show()
			dcm.Show()
			dcm_button.Show()
			sdss.Show()
			sdss_button.Show()
			line4.Show()
			line3.Show()
			energy_pair.Show()
			energy_pair_button.Show()
			confidence.Show()
			confidence_button.Show()
			min_lc.Show()
			min_lc_button.Show()
			line2.Show()
			mismatches.Show()
			max_gc.Show()
			max_gc_button.Show()
			min_gc.Show()
			min_gc_button.Show()
			cutoff_homodimer.Show()
			cutoff_homodimer_button.Show()
			cutoff_hairpin.Show()
			cutoff_hairpin_button.Show()
			max_melt_temp.Show()
			max_melt_temp_button.Show()
			min_melt_temp.Show()
			min_melt_temp_button.Show()
			empty_space2.Show()
			max_prim_size.Show()
			max_prim_size_button.Show()
			min_prim_size.Show()
			min_prim_size_button.Show()
			box_2.Show()
			mismatches_button.Show()
			box_3.Show()
			min_amp.Show()
			min_amp_button.Show()
			max_amp_button.Show()
			cont_button.SetLabel("Less Options")
			panel.SetAutoLayout(1)
			panel.SetupScrolling()
			self.SetSize(680,500)

		else:
			box_4.Hide()
			opr_temp.Hide()
			opr_temp_button.Hide()
			mpc.Hide()
			mpc_button.Hide()
			mcm.Hide()
			mcm_button.Hide()
			dcm.Hide()
			dcm_button.Hide()
			sdss.Hide()
			sdss_button.Hide()
			line4.Hide()
			line3.Hide()
			energy_pair.Hide()
			energy_pair_button.Hide()
			confidence.Hide()
			confidence_button.Hide()
			min_lc.Hide()
			min_lc_button.Hide()
			line2.Hide()
			mismatches.Hide()
			max_gc.Hide()
			max_gc_button.Hide()
			min_gc.Hide()
			min_gc_button.Hide()
			cutoff_homodimer.Hide()
			cutoff_homodimer_button.Hide()
			cutoff_hairpin.Hide()
			cutoff_hairpin_button.Hide()
			max_melt_temp.Hide()
			max_melt_temp_button.Hide()
			min_melt_temp.Hide()
			min_melt_temp_button.Hide()
			empty_space2.Hide()
			max_prim_size.Hide()
			max_prim_size_button.Hide()
			min_prim_size.Hide()
			min_prim_size_button.Hide()
			box_2.Hide()
			min_amp.Hide()
			min_amp_button.Hide()
			mismatches_button.Hide()
			box_3.Hide()
			max_amp_button.Hide()
			cont_button.SetLabel("More Options")
			panel.SetAutoLayout(1)
			panel.SetupScrolling()
			self.SetSize(630,320)



	def onClickDBFILE(self, event):

		print "Clicked"
		#db = wx.FileDialog(self, "Open", "", "", "Fasta Files (*.fasta)|*.fna" ,wx.FD_OPEN)
		#db.ShowModal() # responce
		#print db_button.ShowModal()
		#print db_button.GetPath()
		#print (file.GetPath())
		#db.Destroy()

	def onClickTGFILE(self, e):

		print "Clicked"
		file = wx.FileDialog(self, "Open", "", "", "Text Files (*.*)|*.*" ,wx.FD_OPEN)
		file.ShowModal() # responce
		#print (file.GetPath())
		file.Destroy()

	def onClickRUN(self, e):

		print ("RUNNING")

		# print min_prim_size_button.GetValue()		#1
		# print max_prim_size_button.GetValue()		#2
		# print min_melt_temp_button.GetValue()		#3
		# print max_melt_temp_button.GetValue()		#4
		# print min_gc_button.GetValue()				#5
		# print max_gc_button.GetValue()				#6
		# print min_amp_button.GetValue()				#7
		# print max_amp_button.GetValue()				#8
		# print cutoff_hairpin_button.GetValue()		#9
		# print cutoff_homodimer_button.GetValue()	#10
		# print min_lc_button.GetValue()				#11
		# print mismatches_button.GetValue()			#12
		# print confidence_button.GetValue()			#13
		# print energy_pair_button.GetValue()			#14
		# print opr_temp_button.GetValue()			#15
		# print mpc_button.GetValue()					#16
		# print mcm_button.GetValue()					#17
		# print dcm_button.GetValue()					#18
		# print sdss_button.GetValue()				#19

		arguments = dict()

		arguments ={"min_prim_size":min_prim_size_button.GetValue()}
		arguments ={"max_prim_size":max_prim_size_button.GetValue()}
		arguments ={"min_melt_temp":min_melt_temp_button.GetValue()}
		arguments ={"max_melt_temp":max_melt_temp_button.GetValue()}
		arguments ={"min_gc":min_gc_button.GetValue()}
		arguments ={"max_gc":max_gc_button.GetValue()}
		arguments ={"cutoff_hairpin":cutoff_hairpin_button.GetValue()}
		arguments ={"cutoff_homodimer":cutoff_homodimer_button.GetValue()}
		arguments ={"min_lc":min_lc_button.GetValue()}
		arguments ={"mismatches":mismatches_button.GetValue()}
		arguments ={"confidence":confidence_button.GetValue()}
		arguments ={"energy_pair":energy_pair_button.GetValue()}
		arguments ={"opr_temp":opr_temp_button.GetValue()}
		arguments ={"mpc":mpc_button.GetValue()}
		arguments ={"mcm":mcm_button.GetValue()}
		arguments ={"dcm":dcm_button.GetValue()}
		arguments ={"sdss":sdss_button.GetValue()}

		if min_prim_size_button.GetValue() > max_prim_size_button.GetValue():
			wx.MessageBox("Minimum Primer Size can't be higher than Maximium Primer Size ","Info", wx.OK | wx.ICON_INFORMATION)
			min_prim_size_button.SetValue(18)
			max_prim_size_button.SetValue(22)

		if min_melt_temp_button.GetValue() > max_melt_temp_button.GetValue():
			wx.MessageBox("Minimum Melting Temperature can't be higher than Maximium Melting Temperature ","Info", wx.OK | wx.ICON_INFORMATION)
			min_melt_temp_button.SetValue(60)
			max_melt_temp_button.SetValue(65)

		if min_gc_button.GetValue() > max_gc_button.GetValue():
			wx.MessageBox("Minimum GC Content can't be higher than Maximium GC Content ","Info", wx.OK | wx.ICON_INFORMATION)
			min_gc_button.SetValue(30)
			max_gc_button.SetValue(70)

		if min_amp_button.GetValue() > max_amp_button.GetValue():
			wx.MessageBox("Minimum Amplicon Size can't be higher than Maximium Amplicon Size ","Info", wx.OK | wx.ICON_INFORMATION)
			min_amp_button.SetValue(100)
			max_amp_button.SetValue(300)
	
	def Quit(self,e):
		self.Close()


def main():

	app = wx.App(False,)
	windowClass(None,"ASDADS")

	app.MainLoop()

main()






